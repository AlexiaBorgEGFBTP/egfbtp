<?php
namespace AppBundle\Command;

use AppBundle\Entity\Attestation;
use AppBundle\Entity\Image;
use BackendBundle\Entity\EmailJob;
use BackendBundle\Entity\Jobs;
use FOS\UserBundle\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\User;

class RegenerateGeocodeCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('app:regenerate:geocode')
             ->setDescription('Regenerer les Geocode des utilisateurs OF');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var \UserBundle\Repository\UserRepository $userRepository */
        $userRepository = $em->getRepository(User::class);
        $users = $userRepository->findByRole('ROLE_OF');
        /** @var \AppBundle\Services\QCM $service */
        $service = $this->getContainer()->get('app.services.qcm');
        /** @var \UserBundle\Entity\User $user */
        foreach ($users as $user) {
            if($user->getAdresse()) {
                $response  = $service->geocode($user->getAdresse());
                if($response) {
                    $user->setLatitude($response['latitude']);
                    $user->setLongitude($response['longitude']);
                    if(!empty($response['postal_code']))
                        $user->setCodePostal($response['postal_code']);
                    if(!empty($response['city']))
                        $user->setCity($response['city']);
                    $em->persist($user);
                    $em->flush();
                }
                $output->writeln('<info>'.$user->getEmail().' OK </info>');
            }
        }
    }

}