<?php
namespace AppBundle\Command;

use AppBundle\Entity\Attestation;
use AppBundle\Entity\Image;
use BackendBundle\Entity\EmailJob;
use BackendBundle\Entity\Jobs;
use FOS\UserBundle\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\Apprenant;

class DeletedOldApprenantsCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('app:delete:old-apprenants')
             ->setDescription('Effacer définitivement les apprenants après 10 ans d\'archivage.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var \UserBundle\Repository\ApprenantRepository $apprenantsRepository */
        $apprenantsRepository = $em->getRepository(Apprenant::class);
        $apprenants = $apprenantsRepository->getArchivedApprenantsToDelete();
        if(count($apprenants) > 0) {
            $output->writeln('<info>['.(new \DateTime('now'))->format('Y-m-d H:i:s').'] Suppression de '.count($apprenants).' Apprenant</info>');
            /** @var \UserBundle\Entity\Apprenant $apprenant */
            foreach ($apprenants as $apprenant) {
                $output->writeln('<info>'.$apprenant->getCode().' deleted</info>');
                $em->remove($apprenant);
            }
            $em->flush();
        }
    }

}