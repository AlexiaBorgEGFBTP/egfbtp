<?php
namespace AppBundle\Command;

use AppBundle\Entity\Attestation;
use AppBundle\Entity\Image;
use BackendBundle\Entity\EmailJob;
use BackendBundle\Entity\Jobs;
use FOS\UserBundle\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use UserBundle\Entity\Apprenant;

class RegeneratePdfCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('app:regenerate:apprenants')
             ->setDescription('Regenerer les PDFs importer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.passeport-securite-interim-btp.fr');
        $context->setScheme('http');
        $context->setBaseUrl('/');


        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var \UserBundle\Repository\ApprenantRepository $apprenantsRepository */
        $apprenantsRepository = $em->getRepository('UserBundle:Apprenant');
        $apprenants = $apprenantsRepository->findBy(['imported' => true]);
        $count = count($apprenants);
        $i=1;
        foreach ($apprenants as $apprenant) {
            $html  = $this->getContainer()->get('templating')->render("AppBundle:Attestation:view.html.twig",['apprenant' => $apprenant, 'print' => false]);
            $filename = $apprenant->getCode().'.pdf';
            $attestationDir = $this->getContainer()->get('kernel')->getRootDir().'/../web/images/';
            if(file_exists($attestationDir.$filename))
                unlink($attestationDir.$filename);
            $this->getContainer()->get('knp_snappy.pdf')->generateFromHtml($html, $attestationDir.$filename);

            $image = $apprenant->getAttestation()->getImageOF();
            if(!$image) {
                $image = new Image();
                $image->setAlt($apprenant->getCode());
                $image->setOriginalName($filename);
                $image->setFilename($filename);
                $image->setPath('images/'.$filename);
                $image->setExtension('pdf');
                $apprenant->getAttestation()->setImageOF($image);

                $em->persist($apprenant);
                $em->flush();

            } else {
                $image->setOriginalName($filename);
                $image->setFilename($filename);
                $image->setPath('images/'.$filename);

                $em->persist($image);
                $em->flush();
            }

            $output->writeln('<info>'.$i.'/'.$count.' - '.$apprenant->getCode().'</info>');
            $i++;
        }
    }

}