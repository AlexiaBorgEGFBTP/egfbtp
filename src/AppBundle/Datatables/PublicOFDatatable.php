<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;
use UserBundle\Repository\LoginAttemptRepository;

/**
 * Class OFDatatable
 *
 * @package AppBundle\Datatables
 */
class PublicOFDatatable extends AbstractDatatable
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository(User::class);
        $formatter = function($line) use ($repository){
            /** @var User $user */
            $user = $repository->find($line['id']);
            $line['adresse']  = $user->getAdresse() ;
            $line['codePostal']  = $user->getCodePostal() ;
            $line['city']  = $user->getCity() ;
            if($user->getCity()) {
                $line['adresse'].= ', '.$user->getCity().' '.$user->getCodePostal();
            }
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {

        $this->callbacks->set(array(
            'init_complete' => array(
                'template' => 'AppBundle:Admin:Users/init.js.twig',
            ),
        ));

        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'fr'
        ));

        $this->ajax->set(
            array(
                'url' => $this->router->generate('public_api_of'),
                'type' => 'GET',
            )
        );

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => array(array(0, 'desc')),
            'order_cells_top' => true,
            'search_in_non_visible_columns' => true,
            'page_length' => 100,
            'length_menu' => [100, 200, 300]
        ));

        $this->columnBuilder
            ->add('id',Column::class,['visible' => false])
            ->add('societe', Column::class, ['title'=> "Organisme", 'default_content' => '-'])
            ->add('adresse', VirtualColumn::class, [
                'title'=> "Adresse",
                'default_content' => '-',
                'searchable'        => true,
                'orderable'         => true,
                'order_column' => 'adresse',
                'search_column' => 'adresse'
            ])
            ->add('codePostal', Column::class, [ 'title' => "Code postal", 'default_content' => '-'])
            ->add('city', Column::class, [ 'title' => "Ville", 'default_content' => '-'])
            ->add('telephone', Column::class, [ 'title'=> "Téléphone", 'default_content' => '-'])
            ->add('email', Column::class, [ 'title' => "Email", 'default_content' => '-'])
            ;

    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'public_of_datatable';
    }
}
