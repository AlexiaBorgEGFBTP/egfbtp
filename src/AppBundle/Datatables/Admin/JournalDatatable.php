<?php

namespace AppBundle\Datatables\Admin;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;

/**
 * Class JournalDatatable
 *
 * @package AppBundle\Datatables
 */
class JournalDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository(LoginAttempt::class);
        $formatter = function($line) use ($repository){
            /** @var LoginAttempt $loginAttempt */
            $loginAttempt = $repository->find($line['id']);
            if($loginAttempt->getIp() == "41.226.4.46") {
                $line['countryCode']  = '<span class="flag-icon flag-icon-fr"></span>';
            } else {
                $line['countryCode']  = $loginAttempt->getCountryCode() ? '<span class="flag-icon flag-icon-'.strtolower($loginAttempt->getCountryCode()).'"></span>' : '-';
            }

            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'fr'
        ));

        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_journal'),
                'type' => 'GET',
            )
        );

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => array(array(0, 'desc')),
            'order_cells_top' => true,
            'search_in_non_visible_columns' => true,
            'page_length' => 100,
            'length_menu' => [100, 200, 300]
        ));

        $this->columnBuilder
            ->add('id',Column::class,['visible' => false])
            ->add('createdAt',DateTimeColumn::class,[
                'title' => "Date de tentative",
                'date_format' => 'DD/MM/YYYY HH:mm:ss',
                'searchable' => true,
                'orderable' => true,
            ])
            ->add('email', Column::class, [ 'title' => "Email"])
            ->add('ip', Column::class, [ 'title' => "IP"])
            ->add('countryCode', VirtualColumn::class, [ 'title' => "Pays"])
            ;
    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return LoginAttempt::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'login_attempts_datatable';
    }
}
