<?php

namespace AppBundle\Datatables\Admin;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;
use UserBundle\Repository\LoginAttemptRepository;

/**
 * Class OFDatatable
 *
 * @package AppBundle\Datatables
 */
class OFDatatable extends AbstractDatatable
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository(User::class);
        /** @var LoginAttemptRepository $loginAttemptRepository */
        $loginAttemptRepository = $this->em->getRepository(LoginAttempt::class);
        $formatter = function($line) use ($repository, $loginAttemptRepository){
            /** @var User $user */
            $user = $repository->find($line['id']);
            $attemptsCount = $loginAttemptRepository->findBy(['email' => $user->getEmail(), 'authorizedByAdmin' => false, 'successAttempt' => false]);
            $line['isEnabled']  = $user->isEnabled() && count($attemptsCount) < 3 ;
            $line['adresse']  = $user->getAdresse() ;
            $line['attemptsCount']  = count($attemptsCount) ;
            if($user->getCity()) {
                $line['adresse'].= ', '.$user->getCity().' '.$user->getCodePostal();
            }
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {

        $this->callbacks->set(array(
            'init_complete' => array(
                'template' => 'AppBundle:Admin:Users/init.js.twig',
            ),
        ));

        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'fr'
        ));

        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_of'),
                'type' => 'GET',
            )
        );

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => array(array(0, 'desc')),
            'order_cells_top' => true,
            'search_in_non_visible_columns' => true,
            'page_length' => 100,
            'length_menu' => [100, 200, 300]
        ));

        $this->columnBuilder
            ->add('id',Column::class,['visible' => false])
            ->add('societe', Column::class, ['title'=> "Société", 'default_content' => '-'])
            ->add('adresse', VirtualColumn::class, [
                'title'=> "Adresse",
                'default_content' => '-',
                'searchable'        => true,
                'orderable'         => true,
                'order_column' => 'adresse',
                'search_column' => 'adresse'
            ])
            ->add('telephone', Column::class, [ 'title'=> "Téléphone", 'default_content' => '-'])
            ->add('email', Column::class, [ 'title' => "Email", 'default_content' => '-'])
            ->add('attemptsCount', VirtualColumn::class, [ 'title' => "Tentatives de connexion"])
            ->add('createdAt',DateTimeColumn::class,[
                'title' => "Date d'inscription",
                'date_format' => 'DD/MM/YYYY',
                'searchable' => true,
                'orderable' => true,
                'default_content' => '-'
            ])
            ->add('emailEnabled', BooleanColumn::class, array(
                'title'             => 'Email activé',
                'searchable'        => true,
                'orderable'         => true,
                'true_label'        => 'Oui',
                'false_label'       => 'Non',
                'default_content'   => '<span class="label label-danger">Non</span>',
                'true_icon'         => 'label label-success',
                'false_icon'        => 'label label-danger'
            ))
            ->add('enabled', BooleanColumn::class, array(
                'title'             => 'Activé',
                'searchable'        => true,
                'orderable'         => true,
                'true_label'        => 'Oui',
                'false_label'       => 'Non',
                'default_content'   => '<span class="label label-danger">Non</span>',
                'true_icon'         => 'label label-success',
                'false_icon'        => 'label label-danger'
            ))
            ->add('hasToChangePassword', BooleanColumn::class, array(
                'title'             => 'Mot de passe initialisé',
                'searchable'        => true,
                'orderable'         => true,
                'true_label'        => 'Non',
                'false_label'       => 'Oui',
                'default_content'   => '<span class="label label-danger">Oui</span>',
                'true_icon'         => 'label label-danger',
                'false_icon'        => 'label label-success'
            ))
            ->add(null, ActionColumn::class, [
                'title' => 'Actions',
                'start_html' => '<div class="start_actions">',
                'end_html' => '</div>',
                'add_if' => function () {
                    return $this->authorizationChecker->isGranted('ROLE_ADMIN');
                },
                'actions' => [
                    [
                        'route' => 'homepage',
                        'route_parameters' => [
                            '_switch_user' => 'email'
                        ],
                        'label' => null,
                        'icon' => 'fa fa-exchange',
                        'attributes' => [
                            'title' => 'Changer',
                            'class' => 'btn btn-success btn-xs btn-without-label mb-5',
                        ],
                        'render_if' => function ($row) {
                            return $row['enabled'] && $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN');
                        }
                    ],
                    [
                        'route' => 'admin_users_activate',
                        'label' => '',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'fa fa-check',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Activer',
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button',
                        ],
                        'render_if' => function ($row) {
                            return !$row['isEnabled'];
                        }
                    ],
                    [
                        'route' => 'admin_users_deactivate',
                        'label' => '',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'fa fa-user-times',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Désactiver',
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button',
                        ],
                        'render_if' => function ($row) {
                            return $row['isEnabled'];
                        }
                    ],
                    [
                        'route' => 'admin_users_ett_edit',
                        'label' => '',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'fa fa-pencil',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Modifier',
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button',
                        ]
                    ],
                    [
                        'route' => 'admin_users_delete',
                        'label' => '',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'fa fa-trash-o',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => 'Voulez vous supprimé ?',
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'data-toggle' => 'confirmation',
                        ]
                    ]
                ]
            ]);

    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ett_datatable';
    }
}
