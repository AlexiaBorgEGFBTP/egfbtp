<?php

namespace AppBundle\Datatables\Admin;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;

/**
 * Class AcceptedCGUDatatable
 *
 * @package AppBundle\Datatables
 */
class AcceptedCGUDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'fr'
        ));

        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_archived_cgu'),
                'type' => 'GET',
            )
        );

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'stripe_classes' => [ 'strip1', 'strip2', 'strip3' ],
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => array(array(0, 'desc')),
            'order_cells_top' => true,
            'search_in_non_visible_columns' => true,
            'page_length' => 100,
        ));

        $this->columnBuilder
            ->add('id',Column::class,['visible' => false])
            ->add('email', Column::class, [ 'title' => "Email"])
            ->add('societe', Column::class, [ 'title'=> "Société"])
            ->add('siret', Column::class, [ 'title'=> "N° SIRET"])
            ->add('cguAcceptDate',DateTimeColumn::class,[
                'title' => "Date d'acceptation",
                'date_format' => 'DD/MM/YYYY',
                'searchable' => true,
                'orderable' => true,
            ]);

    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'accepted_cgu_datatable';
    }
}
