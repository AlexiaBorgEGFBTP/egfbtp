<?php

namespace AppBundle\Datatables\Admin;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;

/**
 * Class ApprenantDatatable
 *
 * @package AppBundle\Datatables
 */
class ApprenantDatatable extends AbstractDatatable
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        /** @var ApprenantRepository $repository */
        $repository = $this->em->getRepository('UserBundle:Apprenant');
        $formatter = function($line) use ($repository){
            /** @var Apprenant $apprenant */
            $apprenant = $repository->find($line['id']);
            $line['attestations'] = "Examen pas encore passé";
            $line['imageOF']  = null;
            $line['hasAttestation']  = false;
            $line['onPaper']  = $apprenant->getOnPaper();
            if($apprenant->getAttestation()) {
                $line['hasAttestation']  = true;
                if($apprenant->getAttestation()->getSuccessed()) {
                    $line['attestations'] = "Réussite";
                } else {
                    $line['attestations'] = "Échec";
                }
                if($apprenant->getAttestation()->getImageOF())
                    $line['imageOF']  = true;
            } else {
                $line['attestation']['date'] = null;
            }
            $line['ETT'] = $apprenant->getEtt() ? $apprenant->getEtt()->getSociete() : '-';
            $line['OF']  = $apprenant->getOf() ? $apprenant->getOf()->getSociete() : '-';


            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->callbacks->set(array(
            'init_complete' => array(
                'template' => 'AppBundle:Admin:Users/init.js.twig',
            ),
        ));

        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'fr'
        ));

        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_apprenants'),
                'type' => 'GET',
            )
        );

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'stripe_classes' => [ 'strip1', 'strip2', 'strip3' ],
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order' => array(array(0, 'desc')),
            'order_cells_top' => true,
            'search_in_non_visible_columns' => true,
            'length_menu' => [100, 200, 300],
            'page_length' => 100,
        ));

        $this->features->set(array(
        ));

        $isOF = $this->securityToken->getToken()->getUser()->hasRole('ROLE_OF');
        $this->columnBuilder
            ->add('id',Column::class,['visible' => false])
            ->add('nom',Column::class,['visible' => false])
            ->add('prenom',Column::class,['visible' => false])
            ->add('code', Column::class, [ 'title'         => "Code"])
            ->add('full_name', Column::class, array(
                'title' => 'Nom & prénom',
                'dql' => "CONCAT(apprenant.nom, ' ', apprenant.prenom)",
                'searchable' => true,
                'orderable' => true,
            ))
            ->add('birthday',DateTimeColumn::class,[
                'title' => "Date de naissance",
                'date_format' => 'DD/MM/YYYY',
                'searchable' => true,
                'orderable' => true,
            ])
//            ->add('attestations', BooleanColumn::class, array(
//                'title' => 'Attestation',
//                'searchable' => true,
//                'orderable' => true,
//                'true_label' => null,
//                'false_label' => null,
//                'default_content' => 'Default Value',
//                'true_icon' => 'glyphicon glyphicon-ok',
//                'false_icon' => 'glyphicon glyphicon-remove'
//            ))
            ->add('attestations', VirtualColumn::class, [ 'title' => "Attestation"])
            ->add('attestation.date',DateTimeColumn::class,[
                'title' => "Date d'examen",
                'date_format' => 'DD/MM/YYYY',
                'default_content' => '-',
                'searchable' => true,
                'orderable' => true
            ])
                ;

        if(!$isOF)
            $this->columnBuilder
            ->add('OF', VirtualColumn::class, array(
                'title' => 'OF',
                'searchable' => false,
                'orderable' => false,
                'order_column'  => 'off.societe',
                'search_column' => 'off.societe',
            ));

        $this->columnBuilder
            ->add('ETT', VirtualColumn::class, array(
                'title' => 'ETT',
                'searchable' => true,
                'orderable' => true,
                'order_column'  => 'ett.societe',
                'search_column' => 'ett.societe',
            ));

        if($isOF) {
            $this->columnBuilder
                ->add(null, ActionColumn::class, array(
                    'title' => 'Actions',
                    'start_html' => '<div class="actions">',
                    'end_html' => '</div>',
                    'actions' => array(
                        array(
                            'route' => 'image_of_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return $row['hasAttestation'] && $row['imageOF'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-download',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank',
                                'download' => 'download'
                            )
                        ),
                        array(
                            'route' => 'attestation_view',
                            'route_parameters' => array(
                                'code'  => 'code'
                            ),
                            'render_if' => function($row) {
                                return $row['hasAttestation'] && !$row['imageOF'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-file-pdf-o',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'attestation_view',
                            'route_parameters' => array(
                                'code'  => 'code',
                                'print' => true
                            ),
                            'render_if' => function($row) {
                                return $row['hasAttestation'] && !$row['imageOF'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-print',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'of_edit_manuelle_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return
                                    ($row['hasAttestation']) ||
                                    (!$row['hasAttestation'] && $row['onPaper']);
                            },
                            'label' => null,
                            'icon' => 'fa fa-pencil',
                            'attributes' => array(
                                'title' => null
                            )
                        ),
                        array(
                            'route' => 'print_qcm',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return !$row['hasAttestation'] && $row['onPaper'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-print',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'attestation_start',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return !$row['hasAttestation'] && !$row['onPaper'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-caret-square-o-right',
                        ),
                        array(
                            'route' => 'of_edit_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return !$row['hasAttestation'] && !$row['onPaper'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-pencil',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'of_delete_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'label' => null,
                            'icon' => 'fa fa-trash-o',
                            'confirm' => true,
                            "confirm_message" => "Êtes-vous sure?"
                        )
                    )
                ))
            ;
        } else {
            $this->columnBuilder
                ->add(null, ActionColumn::class, array(
                    'title' => 'Actions',
                    'start_html' => '<div class="actions">',
                    'end_html' => '</div>',
                    'add_if' => function () {
                        return $this->authorizationChecker->isGranted('ROLE_ADMIN');
                    },
                    'actions' => array(
                        array(
                            'route' => 'image_of_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return $row['imageOF'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-eye',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'of_edit_manuelle_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return $row['imageOF'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-pencil',
                            'attributes' => array(
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'attestation_start',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return !$row['hasAttestation'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-caret-square-o-right',
                            'attributes' => array(
//                                'data-toggle' => "confirmation_exam",
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'of_edit_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'render_if' => function($row) {
                                return !$row['hasAttestation'];
                            },
                            'label' => null,
                            'icon' => 'fa fa-pencil',
                            'attributes' => array(
                                'data-toggle' => "confirmation_exam",
                                'title' => null,
                                'target' => '_blank'
                            )
                        ),
                        array(
                            'route' => 'of_delete_apprenant',
                            'route_parameters' => array(
                                'code' => 'code'
                            ),
                            'label' => null,
                            'icon' => 'fa fa-trash-o',
                            'confirm' => true,
                            "confirm_message" => "Êtes-vous sure?"
                        )
                    )
                ))
            ;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Apprenant::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'apprenant_datatable';
    }
}
