<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AttachmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $constraints= [];
        if($options['mimeTypes']) {
            $constraints = [
                new Assert\File( [ "mimeTypes" => $options['mimeTypes'] ] )
            ];
        }
        $builder
            ->add('file', FileType::class, [
                'label' => false,
                'error_bubbling' => true,
                'constraints' => $constraints
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => File::class,
            'mimeTypes' => null,
            'translation_domain' => 'signalement'
        ));
    }
}