<?php

namespace AppBundle\Form\Type;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints as Asserts;
use UserBundle\Entity\User;

class AssistanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('societe',  TextType::class, [
                'label' => 'Nom et prénom',
                'constraints' => [ new Asserts\NotBlank(['message' => 'Ce champs est obligatoire.'])]
            ])
            ->add('telephone',  TextType::class, [
                'label' => 'Téléphone',
                'required' => true,
                'constraints' => [ new Asserts\NotBlank(['message' => 'Ce champs est obligatoire.'])]
            ])
            ->add('email', EmailType::class,[
                'label' => 'E-mail',
                'constraints' => [
                    new Asserts\Email(['message' => 'L\'adresse email est invalide.']),
                    new Asserts\NotBlank(['message' => 'Ce champs est obligatoire.'])
                ]
            ])
            ->add('roles', ChoiceType::class,
                [
                    'label' => "Niveau d'autorisation",
                    'choices'  => [
                        'Accès non autorisé' => 'ROLE_ADMIN_EMPTY',
                        'Autorisé à consulter' => 'ROLE_ADMIN_VIEWER',
                        'Autorisé à modifier/tester' => 'ROLE_ADMIN'
                    ],
                    'empty_data' => null,
                    'required' => true,
                    'mapped' => false,
                    'constraints' => [
                        new Asserts\NotBlank(['message' => 'Ce champs est obligatoire.'])
                    ]
                ]
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => User::class,
           'edit'       => false
        ]);
    }

}