<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApprenantImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('file', AttachmentType::class, [
                'required' => false,
                'attr'     => [
                    'class'  => 'filestyle',
                    'data-classButton'  => 'btn btn-primary',
                    'data-buttonText'  => 'Importer des apprenants',
                ],
                "mimeTypes" => ['text/csv','text/plain'],
                'error_bubbling' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => null
        ));
    }

    public function getName()
    {
        return 'apprenant_import';
    }
}