<?php
namespace AppBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 * Class ForceBrute
 * @package AppBundle\Form\Validator\Constraints
 */
class ForceBrute extends Constraint
{
    public $message = 'Attention il ne vous reste plus que %nbr% tentatives';
    public $message2 = 'Veuillez contacter l’administrateur xxxx@passeportsecuritéinterim.fr pour effectuer une nouvelle demande d’accès à la plateforme';
}