<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\Admin\AcceptedCGUDatatable;
use AppBundle\Datatables\Admin\ArchivedApprenantDatatable;
use AppBundle\Datatables\Admin\JournalDatatable;
use AppBundle\Entity\Attestation;
use AppBundle\Form\Type\QCMExamType;
use AppBundle\Form\Type\UserETTType;
use AppBundle\Form\Type\UserOFType;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Form\ApprenantType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JournalController
 * @package AppBundle\Controller
 * @Route("/admin/journal")
 */
class JournalController extends Controller
{
    /**
     * @Route("/journal", name="admin_journal_recensement")
     * @Template("AppBundle:Admin:Journal\index.html.twig")
     */
    public function listAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(JournalDatatable::class);
        $datatable->buildDatatable();

        /** @var \UserBundle\Repository\LoginAttemptRepository $loginAttemptsRepository */
        $loginAttemptsRepository = $this->getDoctrine()->getRepository(LoginAttempt::class);
        $failedLoginAttemptsCount = $loginAttemptsRepository->getFailedLoginAttemptsCount();
        $failedLoginAttemptsPerDay = $loginAttemptsRepository->getFailedLoginAttemptsPerDay();
        $spyAttempts = $loginAttemptsRepository->getSpyAttempts();

        return [
            'datatable' => $datatable,
            'spyAttempts' => $spyAttempts,
            'failedLoginAttemptsCount' => $failedLoginAttemptsCount,
            'failedLoginAttemptsPerDay' => $failedLoginAttemptsPerDay
        ];
    }
}
