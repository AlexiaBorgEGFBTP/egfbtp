<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Entity\Attestation;
use AppBundle\Entity\Image;
use AppBundle\Form\Type\ApprenantImportType;
use AppBundle\Form\Type\UserETTType;
use AppBundle\Form\Type\UserOFType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\DateTime;
use UserBundle\Form\ApprenantType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\User;

/**
 * Class ApprenantController
 * @package AppBundle\Controller
 * @Route("/apprenants")
 */
class ApprenantController extends Controller
{
    /**
     * @Route("/", name="of_list_apprenants")
     * @Template("AppBundle:Apparenants:list.html.twig")
     */
    public function listAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ApprenantDatatable::class);
        $datatable->buildDatatable();

//        $of = $this->getUser();
//        $apprenants = $this->getDoctrine()->getRepository('UserBundle:Apprenant')->findBy(['of' => $of]);
        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/attestation-apprenant/{code}", name="image_of_apprenant")
     */
    public function imageAction(Request $request, Apprenant $apprenant)
    {
        if(!$apprenant->getAttestation() || !$apprenant->getAttestation()->getImageOF())
            return $this->redirect('/',404);

        return $this->redirect('/images/'.$apprenant->getAttestation()->getImageOF()->getFilename());
    }

    /**
     * @Route("/ajouter", name="of_add_apprenant")
     * @Template("AppBundle:Apparenants:add.html.twig")
     */
    public function ajouterApprenantAction(Request $request)
    {
        $of = $this->getUser();
        $apprenantExist = null;
        $apprenant = new Apprenant();
        $apprenant->setOf($of);
        $form = $this->createForm(ApprenantType::class,$apprenant);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $apprenantRepository = $em->getRepository('UserBundle:Apprenant');
            $apprenantExist = $apprenantRepository->findOneBy([
               'nom'      => $apprenant->getNom(),
               'prenom'   => $apprenant->getPrenom(),
               'birthday' => $apprenant->getBirthday()
            ]);

            if(!$apprenantExist) {
                $code = $this->get('app.apprenant.code')->generateUnexistantCode(['length' => 16]);
                $apprenant->setCode($code);
                $em->persist($apprenant);
                $em->flush();

                $session = $request->getSession();
                $session->getFlashBag()
                    ->add(
                        'success',
                        'L\'apprenant '.$apprenant->getPrenom().' '.$apprenant->getNom().' a été ajouté !'
                    );

                return $this->redirectToRoute('of_list_apprenants');
            }
        }
        return [
            'form'           => $form->createView(),
            'apprenantExist' => $apprenantExist,
            'isManually' => false
        ];
    }

    /**
     * @Route("/editer/{code}", name="of_edit_apprenant")
     * @Template("AppBundle:Apparenants:edit.html.twig")
     */
    public function editerApprenantAction(Request $request, Apprenant $apprenant)
    {
        if($apprenant->getAttestation()) return $this->redirectToRoute('of_edit_manuelle_apprenant',['code' => $apprenant->getCode() ]);
        $apprenantExist = null;
        $form = $this->createForm(ApprenantType::class,$apprenant);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $apprenantRepository = $em->getRepository('UserBundle:Apprenant');
            $apprenantExist = $apprenantRepository->findOneBy([
               'nom'      => $apprenant->getNom(),
               'prenom'   => $apprenant->getPrenom(),
               'birthday' => $apprenant->getBirthday()
            ]);

            if($form->isSubmitted() && $form->has('generateAttestation') && $form->get('generateAttestation')->isClicked()) {
                $attestation = $apprenant->getAttestation();
                if(!$apprenant->getAttestation()) $attestation = new Attestation();

                $succecced = $request->request->get('apprenant')['succceced'];
                $date = $request->request->get('apprenant')['date'];
                $date = \DateTime::createFromFormat('d/m/Y',$date);
                $attestation->setSuccessed($succecced);
                $attestation->setDate($date);
                if(!$apprenant->getAttestation()) $apprenant->setAttestation($attestation);

                $em->persist($apprenant);
                $em->flush();

                return $this->redirectToRoute('attestation_view', ['code' => $apprenant->getCode()]);
            }

            if($apprenantExist && $apprenantExist == $apprenant) {
                $em->persist($apprenant);
                $em->flush();

                $session = $request->getSession();
                $session->getFlashBag()
                    ->add(
                        'success',
                        'Les informations de l\'apprenant '.$apprenant->getPrenom().' '.$apprenant->getNom().' ont été mis a jour !'
                    );

                return $this->redirectToRoute('of_list_apprenants');
            }
        }
        return [
            'form'           => $form->createView(),
            'edit'           => true,
            'apprenantExist' => $apprenantExist,
            'isManually' => false
        ];
    }

    /**
     * @Route("/ajouter-manuellement", name="of_add_manuelle_apprenant")
     * @Template("AppBundle:Apparenants:add.html.twig")
     */
    public function ajouterApprenantManuellementAction(Request $request)
    {
        if(!$this->isGranted('ROLE_OF'))
            return $this->redirectToRoute('homepage');

        $session = $request->getSession();
        $apprenantExist = null;
        $isAlreadyExist = false;

        $apprenant = new Apprenant();
        $apprenant->setOf($this->getUser());
        $code = $this->get('app.apprenant.code')->generateUnexistantCode(['length' => 16]);
        $apprenant->setCode($code);
        $attestation = new Attestation();
        $apprenant->setAttestation($attestation);

        $form = $this->createForm(ApprenantType::class,$apprenant,['add_result' => true]);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $apprenantRepository = $em->getRepository('UserBundle:Apprenant');
            $apprenantExist = $apprenantRepository->findOneBy([
                'nom'      => $apprenant->getNom(),
                'prenom'   => $apprenant->getPrenom(),
                'birthday' => $apprenant->getBirthday()
            ]);

            if($form->isSubmitted()) {
                if($imageId = $session->get('apprenant-manuelle', null)) {
                    /** @var \AppBundle\Repository\ImageRepository $imageRepository */
                    $imageRepository = $em->getRepository(Image::class);
                    $image = $imageRepository->find($imageId);
                    $apprenant->getAttestation()->setImageOF($image);
                    $em->persist($apprenant);
                    $em->flush();
                    $session->remove('apprenant-manuelle');
                }
            }

            if($form->isSubmitted() && $form->has('generateAttestation') && $form->get('generateAttestation')->isClicked()) {
                $attestation = $apprenant->getAttestation();
                if(!$apprenant->getAttestation()) $attestation = new Attestation();

                $succecced = $request->request->get('apprenant')['succceced'];
                $date = $request->request->get('apprenant')['date'];
                $date = \DateTime::createFromFormat('d/m/Y',$date);
                $attestation->setSuccessed($succecced);
                $attestation->setDate($date);
                if(!$apprenant->getAttestation()) $apprenant->setAttestation($attestation);

                $em->persist($apprenant);
                $em->flush();

                return $this->redirectToRoute('attestation_view', ['code' => $apprenant->getCode()]);
            }
            elseif(!$apprenantExist ||($apprenantExist && $apprenantExist == $apprenant)) {

                $attestation = $apprenant->getAttestation();
                $succecced = $request->request->get('apprenant')['succceced'];
                $date = $request->request->get('apprenant')['date'];
                $date = \DateTime::createFromFormat('d/m/Y',$date);
                $attestation->setSuccessed($succecced);
                $attestation->setDate($date);

                $em->persist($apprenant);
                $em->flush();

                $session->getFlashBag()
                    ->add(
                        'success',
                        'Les informations de l\'apprenant '.$apprenant->getPrenom().' '.$apprenant->getNom().' ont été modifier !'
                    );
                return $this->redirectToRoute('of_list_apprenants');
            }  else {
                if($form->isSubmitted() & $form->get('affectResult')->isClicked()){

                    $attestation = $apprenantExist->getAttestation();
                    if(!$apprenantExist->getAttestation()) $attestation = new Attestation();

                    $succecced = $request->request->get('apprenant')['succceced'];
                    $date = $request->request->get('apprenant')['date'];
                    $attestation->setSuccessed($succecced);
                    $attestation->setDate($date);
                    $apprenantExist->setAttestation($attestation);

                    $em->persist($apprenantExist);
                    $em->flush();

                    $session->getFlashBag()->add('success','la résultat a été affecté !');
                    return $this->redirectToRoute('of_list_apprenants');
                }
            }
        }
        return [
            'form'           => $form->createView(),
            'apprenantExist' => $apprenantExist,
            'apprenant'      => $apprenant,
            'isAlreadyExist' => $isAlreadyExist,
            'isManually' => true
        ];

    }

    /**
     * @Route("/editer-manuellement/{code}", name="of_edit_manuelle_apprenant")
     * @Template("AppBundle:Apparenants:add.html.twig")
     */
    public function editerApprenantManuellementAction(Request $request, Apprenant $apprenant)
    {
        if(!$this->isGranted('ROLE_OF'))
            return $this->redirectToRoute('homepage');

        $session = $request->getSession();
        $apprenantExist = null;
        $isAlreadyExist = false;

        $form = $this->createForm(ApprenantType::class,$apprenant,['add_result' => true]);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($form->isSubmitted() && $form->has('generateAttestation') && $form->get('generateAttestation')->isClicked()) {
                $attestation = $apprenant->getAttestation();
                if(!$apprenant->getAttestation()) $attestation = new Attestation();

                $succecced = $request->request->get('apprenant')['succceced'];
                $date = $request->request->get('apprenant')['date'];
                $date = \DateTime::createFromFormat('d/m/Y',$date);
                $attestation->setSuccessed($succecced);
                $attestation->setDate($date);
                if(!$apprenant->getAttestation()) $apprenant->setAttestation($attestation);

                $em->persist($apprenant);
                $em->flush();

                return $this->redirectToRoute('attestation_view', ['code' => $apprenant->getCode()]);
            }  else {
                $attestation = $apprenant->getAttestation();
                if(!$apprenant->getAttestation()) $attestation = new Attestation();

                $succecced = $request->request->get('apprenant')['succceced'];
                $date = $request->request->get('apprenant')['date'];
                $attestation->setSuccessed($succecced);
                $attestationDate = \DateTime::createFromFormat('d/m/Y', $date);
                $attestation->setDate($attestationDate);
                $apprenant->setAttestation($attestation);

                $em->persist($apprenant);
                $em->flush();

                $this->addFlash('success','la résultat a été affecté !');
                return $this->redirectToRoute('of_list_apprenants');
            }
        }
        return [
            'form'           => $form->createView(),
            'apprenant'      => $apprenant,
            'isAlreadyExist' => $isAlreadyExist,
            'isManually' => true
        ];
    }

    /**
     * @Route("/delete/{code}", name="of_delete_apprenant")
     */
    public function deleteApprenantAction(Request $request,Apprenant $apprenant)
    {
        $user = $this->getUser();
        $session = $request->getSession();

        if(!$this->isGranted('ROLE_ADMIN'))
        if($user != $apprenant->getOf()) {
            $session->getFlashBag()->add( 'error', 'Vous n\'êtes pas autorisez a supprimer cette apprenant !' );
            return $this->redirectToRoute('of_list_apprenants');
        }

        $session->getFlashBag()->add( 'success', 'L\'apprenant ' . $apprenant->getNom() . ' '.$apprenant->getPrenom().' a bien été supprimé !' );
        $em = $this->getDoctrine()->getManager();
        $em->remove($apprenant);
        $em->flush();
        // Message de validation
        if($this->isGranted('ROLE_ADMIN'))
            return $this->redirectToRoute('admin_list_apprenants');
        return $this->redirectToRoute('of_list_apprenants');
    }
}
