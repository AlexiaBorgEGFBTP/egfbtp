<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Datatables\Admin\BTPDatatable;
use AppBundle\Datatables\Admin\ETTDatatable;
use AppBundle\Datatables\Admin\OFDatatable;
use AppBundle\Entity\Attestation;
use AppBundle\Entity\Image;
use AppBundle\Form\Type\AddUserOFType;
use AppBundle\Form\Type\ApprenantImportType;
use AppBundle\Form\Type\AssistanceType;
use AppBundle\Form\Type\UserETTType;
use AppBundle\Form\Type\UserOFType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Entity\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends Controller
{
    /**
     * @Route("/admin/utilisateurs/apprenants", name="admin_list_apprenants")
     * @Template("AppBundle:Admin:Users/list_apprenants.html.twig")
     */
    public function usersApprenantsAction(Request $request)
    {
        $apprenantsCount = $this->getDoctrine()->getRepository('UserBundle:Apprenant')->count();
        $formImport = $this->createForm(ApprenantImportType::class);

        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ApprenantDatatable::class);
        $datatable->buildDatatable();

        if ($formImport->handleRequest($request) && $formImport->isSubmitted() && $request->isXmlHttpRequest()) {
            if($formImport->isValid()) {
                $em     = $this->getDoctrine()->getManager();
                /** @var \UserBundle\Repository\ApprenantRepository $apprenantsRepository */
                $apprenantsRepository = $em->getRepository('UserBundle:Apprenant');

                /** @var \UserBundle\Repository\UserRepository $userRepository */
                $userRepository = $em->getRepository('UserBundle:User');

                $data = $formImport->getData();
                /** @var UploadedFile $file */
                $file = $data['file']->getFile();

                $fileName = $file->getClientOriginalName();
                $path = $this->getParameter("kernel.root_dir").'/../vars/';
                $file->move( $path , $fileName );
                $filePath = $path.$fileName;

                $csvFile = file("{$filePath}");

                $lines = [];
                $first = true;
                $deliminator = ',';
                if(isset($csvFile[0])) {
                    $expLine = str_getcsv($csvFile[0]);
                    if(count($expLine) == 1) {
                        $deliminator = ';';
                    }
                }

                foreach ($csvFile as $line)
                {
                    if($first) {
                        $first = false;
                        continue;
                    }
                    $lines[] = str_getcsv($line, $deliminator);
                }
                $k=0;
                $i=0;
                $j=0;
                $m=0;
                $codes = [];
                $OFs   = [];
                $today = new \DateTime('today');
                foreach ($lines as $line) {
                    $OF = null;
                    $line[7] = trim($line[7]);
                    if(!isset($OFs[$line[7]])) {
                        $OF = $userRepository->findOneBy(['email' => $line[7]]);
                        $OFs[$line[7]] = $OF;
                    } else {
                        $OF = $OFs[$line[7]];
                    }

                    if(!is_null($OF)) {
                        $apprenant = $apprenantsRepository->findOneBy(['code' => $line[0]]);
                        if(!$apprenant && !in_array($line[0],$codes)) {
                            try {
                                $i++;
                                $k++;
                                $birthday = $attestationDate = null;
                                $attestation = new Attestation();
                                if(!is_bool($line[6]) && !is_bool($line[3])) {
                                    $attestationDate = \DateTime::createFromFormat('m/d/Y', $line[6]);
                                    if(!$attestationDate || $attestationDate > $today) {
                                        $attestationDate = \DateTime::createFromFormat('d/m/Y', $line[6]);
                                    }
                                    $attestation->setDate($attestationDate instanceof \DateTime ? $attestationDate : new \DateTime("now"))
                                        ->setSuccessed(trim($line[5]) == 'Réussite' || trim($line[5]) == 'R‚ussite' ? true : false);

                                    $birthday = !empty($line[3]) ? \DateTime::createFromFormat('m/d/Y', $line[3]) : null;
                                    if(!$birthday || $birthday > $today) {
                                        $birthday = \DateTime::createFromFormat('d/m/Y', $line[3]);
                                    }
                                    $apprenant = new Apprenant();
                                    $apprenant->setAttestation($attestation)
                                        ->setCode($line[0])
                                        ->setNom($line[1])
                                        ->setPrenom($line[2])
                                        ->setBirthday($birthday instanceof \DateTime ? $birthday : null)
                                        ->setImported(true)
                                        ->setOf($OF);

                                    $html  = $this->renderView("AppBundle:Attestation:view.html.twig",['apprenant' => $apprenant, 'print' => false]);
                                    $filename = $apprenant->getCode().'.pdf';
                                    $attestationDir = $this->get('kernel')->getRootDir().'/../web/images/';
                                    if(file_exists($attestationDir.$filename))
                                        unlink($attestationDir.$filename);
                                    $this->get('knp_snappy.pdf')->generateFromHtml($html, $attestationDir.$filename);

                                    $image = new Image();
                                    $image->setAlt($apprenant->getCode());
                                    $image->setOriginalName($filename);
                                    $image->setFilename($filename);
                                    $image->setPath('images/'.$filename);
                                    $image->setExtension('pdf');
                                    $apprenant->getAttestation()->setImageOF($image);

                                    $em->persist($apprenant);
                                    $codes[] = $line[0];
                                    if($k == 100) {
                                        $em->flush();
                                        $k=0;
                                    }
                                }
                            } catch (\Exception $e) {
                                continue;
                            }
                        } else {
                            $j++;
                        }
                    }
                    else {
                        $m++;
                    }
                }
                $em->flush();

                if($i > 0 ) {
                    $this->addFlash('success', $i." apprenants ont été importée.");
                }
                if($j > 0) {
                    $this->addFlash('error', $j." existent déjà.");
                }
                if($m > 0) {
                    $this->addFlash('error', $m." apprenants avec OF introuvable.");
                }
                return new JsonResponse(['success' => true]);
            } else {
                return new JsonResponse(['error' => $formImport->get('file')->getErrors()->current()->getMessage()]);
            }
        }

        return [
            'apprenantsCount' => $apprenantsCount,
            'datatable' => $datatable,
            'formImport' => $formImport->createView()
        ];
    }

    /**
     * @Route("/admin/utilisateurs/ETT", name="admin_users_ett")
     * @Template("AppBundle:Admin:Users/list_ett.html.twig")
     */
    public function usersETTAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ETTDatatable::class);
        $datatable->buildDatatable();

        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/admin/utilisateurs/OF", name="admin_users_of")
     * @Template("AppBundle:Admin:Users/list_of.html.twig")
     */
    public function usersOFAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(OFDatatable::class);
        $datatable->buildDatatable();

        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/admin/utilisateurs/BTP", name="admin_users_btp")
     * @Template("AppBundle:Admin:Users/list_btp.html.twig")
     */
    public function usersBTPAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(BTPDatatable::class);
        $datatable->buildDatatable();

        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/admin/utilisateur/ETT/ajouter", name="admin_users_ett_add")
     * @Template("AppBundle:Admin:Users/add_ett.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function ajouterETTAction(Request $request)
    {
        $user = new User();
        $user->addRole('ROLE_VIEWER');
        $form = $this->createForm(UserETTType::class,$user);
        if ($form->handleRequest($request) && $form->isValid()) {
            $user->setEnabled(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $session = $request->getSession();
            $session->getFlashBag()
                ->add(
                    'success',
                    $user->getSociete() .' a été ajouté !'
                );

            return $this->redirectToRoute('admin_users_ett');

        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/admin/utilisateur/ETT/editer/{id}", name="admin_users_ett_edit")
     * @Template("AppBundle:Admin:Users/edit_ett.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editerETTAction(Request $request, User $user)
    {
        $form = $this->createForm(UserETTType::class,$user,['edit' => true]);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $session = $request->getSession();
            $session->getFlashBag()
                ->add(
                    'success',
                    $user->getSociete() .' a été modifié !'
                );
            return $this->redirectToRoute('admin_users_ett');
        }
        return ['form' => $form->createView(), 'edit' => true];
    }

    /**
     * @Route("/admin/utilisateur/OF/ajouter", name="admin_users_of_add")
     * @Template("AppBundle:Admin:Users/add_of.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function ajouterOFAction(Request $request)
    {
        $user = new User();
        $user->addRole('ROLE_OF');
        $form = $this->createForm(AddUserOFType::class,$user);
        if ($form->handleRequest($request) && $form->isValid()) {
            $user->setEnabled(true);
            $user->setPassword('$%4Random_password');
            $user->setHasToChangePassword(true);
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
            $url = $this->generateUrl('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Vous êtes invité à faire votre inscription.')
                ->setFrom($this->getParameter('from_email'),'Passeport Securite Interim BTP')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'AppBundle:Emails:invitation_register_of.html.twig', [ 'user' => $user, 'url' => $url ]
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            $this->addFlash( 'success', $user->getEmail() .' a été invité !');

            return $this->redirectToRoute('admin_users_of');
        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/admin/utilisateur/OF/editer/{id}", name="admin_users_of_edit")
     * @Template("AppBundle:Admin:Users/edit_of.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editerOFAction(Request $request,User $user)
    {
        $form = $this->createForm(UserOFType::class,$user);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                $user->getSociete() .' a été modifié !'
            );

            return $this->redirectToRoute('admin_users_of');
        }
        return ['form' => $form->createView(), 'edit' => true];
    }


    /**
     * @Route("/admin/utilisateur/BTP/ajouter", name="admin_users_btp_add")
     * @Template("AppBundle:Admin:Users/add_btp.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function ajouterBTPAction(Request $request)
    {
        $user = new User();
        $user->addRole('ROLE_BTP');
        $form = $this->createForm(UserETTType::class,$user);
        if ($form->handleRequest($request) && $form->isValid()) {
            $user->setEnabled(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

           $this->addFlash('success',$user->getSociete() .' a été ajouté !');

            return $this->redirectToRoute('admin_users_btp');
        }
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/admin/utilisateur/BTP/editer/{id}", name="admin_users_btp_edit")
     * @Template("AppBundle:Admin:Users/edit_btp.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editerBTPAction(Request $request,User $user)
    {
        $form = $this->createForm(UserETTType::class,$user,['edit' => true]);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $session = $request->getSession();
            $session->getFlashBag()
                ->add(
                    'success',
                    $user->getSociete() .' a été modifié !'
                );

            return $this->redirectToRoute('admin_users_btp');
        }
        return ['form' => $form->createView(), 'edit' => true];
    }

    /**
     * @Route("/admin/utilisateur/{id}/activation", name="admin_users_activate")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function userActivationAction(Request $request, User $user)
    {
        $user->setEnabled(true);
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);

        $loginAttemptsRepository = $this->getDoctrine()->getRepository(LoginAttempt::class);
        $failedLoginAttempts = $loginAttemptsRepository->findBy(['email' => $user->getEmail(), 'successAttempt' => false, 'authorizedByAdmin' => false]);
        if(count($failedLoginAttempts) > 0) {
            $em = $this->getDoctrine()->getManager();
            /** @var LoginAttempt $loginAttempt */
            foreach ($failedLoginAttempts as $loginAttempt) {
                $loginAttempt->setAdmin($this->getUser());
                $loginAttempt->setAuthorizationDate(new \DateTime('now'));
                $loginAttempt->setAuthorizedByAdmin(true);
                $em->persist($loginAttempt);
            }
            $em->flush();
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Votre inscription est approuvée')
            ->setFrom("info@passeport-securite-interim-btp.fr",'Passeport Securite Interim BTP')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Emails:user_account_confirmation.html.twig', ['user' => $user ]
                ),
                'text/html'
        );
        $this->get('mailer')->send($message);

        $session = $request->getSession();
        $session->getFlashBag()
            ->add(
                'success',
                'L\'utilisateur ' . $user->getSociete() .' a été validé !'
            );

        return $this->redirectToRoute($user->hasRole('ROLE_OF') ? 'admin_users_of' : 'admin_users_ett');
    }

    /**
     * @Route("/admin/utilisateur/{id}/desactivation", name="admin_users_deactivate")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function userDesactivationAction(Request $request, User $user)
    {
        $user->setEnabled(false);
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);

        $session = $request->getSession();
        $session->getFlashBag()
            ->add(
                'success',
                'L\'utilisateur ' . $user->getSociete() .' a été désactivé !'
            );

        return $this->redirectToRoute($user->hasRole('ROLE_OF') ? 'admin_users_of' : 'admin_users_ett');
    }

    /**
     * @Route("/admin/utilisateur/{id}/delete", name="admin_users_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteUserAction(Request $request,User $user)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->deleteUser($user);
        $this->addFlash('success','L\'utilisateur ' . $user->getSociete() . ' a bien été supprimé !');
        return $this->redirectToRoute($user->hasRole('ROLE_OF') ? 'admin_users_of' : 'admin_users_ett');
    }

    /**
     * @Route("/admin/utilisateurs/administrators/add/{id}", defaults={"id" = null}, name="admin_add_administrators")
     * @Template("AppBundle:Admin:Users/add_administrator.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function addAdministratorAction(Request $request, User $user = null)
    {
        $isNew = false;
        if(!$user) {
            $user = new User();
            $user->setPlainPassword('$%4Rrandom_password');
            $isNew = true;
        }

        $form = $this->createForm(AssistanceType::class,$user);
        if ($form->handleRequest($request) && $form->isValid()) {
            $role = $form->get('roles')->getData();
            switch ($role) {
                default:
                case "ROLE_ADMIN_EMPTY":
                $user->setRoles(['ROLE_ADMIN_EMPTY']);
                    $user->setEnabled(false);
                    break;
                case "ROLE_ADMIN":
                    $user->setRoles(['ROLE_ADMIN']);
                    $user->setEnabled(true);
                    break;
                case "ROLE_ADMIN_VIEWER":
                    $user->setRoles(['ROLE_ADMIN_VIEWER']);
                    $user->setEnabled(true);
                    break;
            }

            if($isNew) {
                $user->setHasToChangePassword(true);
                $tokenGenerator = $this->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
                $url = $this->generateUrl('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if($isNew) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Vous êtes invité à faire votre inscription')
                    ->setFrom($this->getParameter('from_email'), 'Passeport Securite Interim BTP')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Emails:invitation_register_of.html.twig', ['user' => $user, 'url' => $url]
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);

                $this->addFlash(
                    'success',
                    $user->getSociete() . ' a été ajouté !'
                );
            } else {
                $this->addFlash(
                    'success',
                    $user->getSociete() . ' a été modifié !'
                );
            }

            return $this->redirectToRoute('admin_list_administrators');
        }

        return [
            'form' => $form->createView(),
            'isNew' => $isNew
        ];
    }

    /**
     * @Route("/admin/utilisateurs/administrators", name="admin_list_administrators")
     * @Template("AppBundle:Admin:Users/list_administrator.html.twig")
     */
    public function usersAdministratorAction(Request $request)
    {
        /** @var \UserBundle\Repository\UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $admins = $userRepository->findByRole('ROLE_ADMIN');

        return [
            'admins' => $admins
        ];
    }

    /**
     * @Route("/admin/utilisateur/delete-admin/{id}", name="admin_users_delete_admin")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deleteAdministratorAction(Request $request,User $user)
    {
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->deleteUser($user);
        $this->addFlash('success','L\'assistant” ' . $user->getSociete() . ' a bien été supprimé !');
        return $this->redirectToRoute('admin_list_administrators');
    }
}
