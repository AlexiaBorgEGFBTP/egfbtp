<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\Admin\AcceptedCGUDatatable;
use AppBundle\Datatables\Admin\ArchivedApprenantDatatable;
use AppBundle\Entity\Attestation;
use AppBundle\Form\Type\QCMExamType;
use AppBundle\Form\Type\UserETTType;
use AppBundle\Form\Type\UserOFType;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\ApprenantType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ArchivedController
 * @package AppBundle\Controller
 * @Route("/admin/archived")
 */
class ArchivedController extends Controller
{
    /**
     * @Route("/CGU", name="admin_archived_cgu")
     * @Template("AppBundle:Admin:Archived\cgu_list.html.twig")
     */
    public function listAcceptedCGUAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(AcceptedCGUDatatable::class);
        $datatable->buildDatatable();

        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/apprenants", name="admin_archived_apprenants")
     * @Template("AppBundle:Admin:Archived\apprenants_list.html.twig")
     */
    public function archivedApprenantsAction(Request $request)
    {
        /** @var \Sg\DatatablesBundle\Datatable\DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ArchivedApprenantDatatable::class);
        $datatable->buildDatatable();

        return [
            'datatable' => $datatable
        ];
    }
}
