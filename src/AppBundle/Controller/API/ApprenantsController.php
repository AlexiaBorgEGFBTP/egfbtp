<?php

namespace AppBundle\Controller\API;

use Amazon\MWSBundle\Entity\Store;
use AppBundle\Datatables\Admin\ApprenantDatatable;
use BackendBundle\Datatables\CustomerDatatable;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api")
 */
class ApprenantsController extends Controller
{
    /**
     * @Route("/apprenants", options={"expose"=true}, name="api_apprenants")
     */
    public function fetchApprenantsAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ApprenantDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();


        $qb->leftJoin('apprenant.of','off');
        $qb->leftJoin('apprenant.ett','ett');

        if(!$this->isGranted('ROLE_ADMIN_VIEWER')) {
            $qb->andWhere($qb->expr()->eq('off.id',$qb->expr()->literal($this->getUser()->getId())));
        }

        $date = new \DateTime('now');
        $date->sub(new \DateInterval('P10Y'));
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->isNull('apprenant.attestation'),
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('apprenant.attestation'),
                    $qb->expr()->gte('attestation.date',$qb->expr()->literal($date->format('Y-m-d H:i:s')))
                )
            )
        );

        return $responseService->getResponse();
    }

}
