<?php

namespace AppBundle\Controller\API;

use Amazon\MWSBundle\Entity\Store;
use AppBundle\Datatables\Admin\AcceptedCGUDatatable;
use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Datatables\Admin\ArchivedApprenantDatatable;
use BackendBundle\Datatables\CustomerDatatable;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin/api")
 */
class ArchivedController extends Controller
{
    /**
     * @Route("/cgu", options={"expose"=true}, name="api_archived_cgu")
     */
    public function fetchAcceptedCGUAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(AcceptedCGUDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->eq('user.CGU',':cgu'))
            ->setParameter('cgu',true);

        return $responseService->getResponse();
    }

    /**
     * @Route("/apprenants", options={"expose"=true}, name="api_archived_apprenants")
     */
    public function fetchApprenantsAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ArchivedApprenantDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();


        $qb->leftJoin('apprenant.of','off');
        $qb->leftJoin('apprenant.ett','ett');

        if(!$this->isGranted('ROLE_ADMIN')) {
            $qb->andWhere($qb->expr()->eq('off.id',':userId'))->setParameter('userId',$this->getUser()->getId());
        }

        $date = new \DateTime('now');
        $date->sub(new \DateInterval('P10Y'));
        $qb->andWhere($qb->expr()->lt('attestation.date',':date'))
            ->setParameter('date',$date);


        return $responseService->getResponse();
    }
}
