<?php

namespace AppBundle\Controller\API;

use Amazon\MWSBundle\Entity\Store;
use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Datatables\Admin\BTPDatatable;
use AppBundle\Datatables\Admin\ETTDatatable;
use AppBundle\Datatables\Admin\OFDatatable;
use BackendBundle\Datatables\CustomerDatatable;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin/api")
 */
class UserController extends Controller
{
    /**
     * @Route("/ett", options={"expose"=true}, name="api_ett")
     */
    public function fetchEttAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ETTDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->like('user.roles',$qb->expr()->literal("%ROLE_VIEWER%")));

        return $responseService->getResponse();
    }

    /**
     * @Route("/of", options={"expose"=true}, name="api_of")
     */
    public function fetchOfAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(OFDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->like('user.roles',$qb->expr()->literal("%ROLE_OF%")));

        return $responseService->getResponse();
    }

    /**
     * @Route("/btp", options={"expose"=true}, name="api_btp")
     */
    public function fetchBTPAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(BTPDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->like('user.roles',$qb->expr()->literal("%ROLE_BTP%")));

        return $responseService->getResponse();
    }

}
