<?php

namespace AppBundle\Controller\API;

use Amazon\MWSBundle\Entity\Store;
use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Datatables\Admin\BTPDatatable;
use AppBundle\Datatables\Admin\ETTDatatable;
use AppBundle\Datatables\Admin\OFDatatable;
use AppBundle\Datatables\PublicOFDatatable;
use BackendBundle\Datatables\CustomerDatatable;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api")
 */
class OfController extends Controller
{
    /**
     * @Route("/of", options={"expose"=true}, name="public_api_of")
     */
    public function fetchOfAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(PublicOFDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->like('user.roles',$qb->expr()->literal("%ROLE_OF%")));

        return $responseService->getResponse();
    }

}
