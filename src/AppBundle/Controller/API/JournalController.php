<?php

namespace AppBundle\Controller\API;

use Amazon\MWSBundle\Entity\Store;
use AppBundle\Datatables\Admin\AcceptedCGUDatatable;
use AppBundle\Datatables\Admin\ApprenantDatatable;
use AppBundle\Datatables\Admin\ArchivedApprenantDatatable;
use AppBundle\Datatables\Admin\JournalDatatable;
use BackendBundle\Datatables\CustomerDatatable;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin/api")
 */
class JournalController extends Controller
{
    /**
     * @Route("/journal", options={"expose"=true}, name="api_journal")
     */
    public function journalAction()
    {

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(JournalDatatable::class);
        $datatable->buildDatatable();

        /** @var DatatableResponse $responseService */
        $responseService = $this->get('sg_datatables.response');
        $responseService->setDatatable($datatable);

        $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
        $qb = $datatableQueryBuilder->getQb();

        $qb->andWhere($qb->expr()->eq('loginattempt.successAttempt',':successAttempt'))
            ->setParameter('successAttempt',false);

        return $responseService->getResponse();
    }
}
