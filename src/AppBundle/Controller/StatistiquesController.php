<?php

namespace AppBundle\Controller;

use Geekdevs\SwiftMailer\Transport\FileTransport;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StatistiquesController
 * @package AppBundle\Controller
 * @Route("/statistiques")
 */
class StatistiquesController extends Controller
{
    /**
     * @Route("/", name="admin_statistiques")
     * @Template("AppBundle:Admin:statistiques.html.twig")
     */
    public function indexAction(Request $request)
    {
        /** @var \UserBundle\Repository\ApprenantRepository $apprenantsRepository */
        $apprenantsRepository  = $this->getDoctrine()->getRepository('UserBundle:Apprenant');
        /** @var \AppBundle\Repository\AttestationsRepository $attestationRepository */
        $attestationRepository  = $this->getDoctrine()->getRepository('AppBundle:Attestation');
        $attestationsGraphs     = $attestationRepository->getAttestationGraph();
        $totalAttestationsCount = $attestationRepository->getTotalAttestationCount();
        $totalAttestationsReussitCount = $attestationRepository->getTotalAttestationReussitCount();

        /** @var \AppBundle\Repository\SearchPerDayRepository $searchPerDayRepository */
        $searchPerDayRepository  = $this->getDoctrine()->getRepository('AppBundle:SearchPerDay');
        $searchsGraphs  = $searchPerDayRepository->getMonthsSearchGraph();
        $searchsGraphs2 = $searchPerDayRepository->getMonthsSearchGraphBTP();
        $apprenantsByOFGraph = $apprenantsRepository->getStatistiquesByOF();



//        $eventDispatcher = new \Swift_Events_SimpleEventDispatcher();
//        $transport = new FileTransport($eventDispatcher, '/home/yesser/PhpstormProjects/EGFBTP/web');
//        $mailer = \Swift_Mailer::newInstance($transport);
//
//        $message = \Swift_Message::newInstance()
//            ->setSubject('Update your payment details now')
//            ->setFrom("info@passeport-securite-interim-btp.fr",'Passeport Securite Interim BTP')
//            ->setTo("chakroun.yesser@gmail.com")
//            ->setBody('Hello test');
//        $this->get('mailer')->send($message);

        return [
            'attestationsGraphs'     => $attestationsGraphs,
            'totalAttestationsCount' => $totalAttestationsCount,
            'totalAttestationsReussitCount' => $totalAttestationsReussitCount,
            'apprenantsByOFGraph'          => $apprenantsByOFGraph,
            'searchsGraphs'          => $searchsGraphs,
            'searchsGraphs2'          => $searchsGraphs2
        ];
    }
}
