<?php
/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 4/12/17
 * Time: 8:13 PM
 */

namespace AppBundle\EventListener;


use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Router;
use UserBundle\Entity\User;

class KernelListener
{
    /** @var Router */
    private $router;

    /** @var Container */
    private $container;

    /**
     * SiretVerifiedListener constructor.
     * @param Router $router
     * @param Container $container
     */
    public function __construct($router, $container)
    {
        $this->router       = $router;
        $this->container    = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $container = $this->container;
        if($container->get('security.token_storage')->getToken()) {
            /** @var User $user */
            $user = $container->get('security.token_storage')->getToken()->getUser();
            $isAdmin = $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
            $request = $event->getRequest();
            $_route  = $request->attributes->get('_route');
            if($user !="anon." && $user->getHasToChangePassword() && $_route !== 'fos_user_resetting_reset') {
                if(!$user->getConfirmationToken()) {
                    /** @var $tokenGenerator TokenGeneratorInterface */
                    $tokenGenerator = $container->get('fos_user.util.token_generator');
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                    $em = $container->get('doctrine')->getManager();
                    $em->persist($user);
                    $em->flush();
                }
                $url = $this->router->generate('fos_user_resetting_reset', ['token' => $user->getConfirmationToken()]);
                $event->setResponse(new RedirectResponse($url));
            }
        }
    }
}