<?php

namespace AppBundle\Services;

use AppBundle\Entity\Question;
use AppBundle\Repository\QCMsQuestionsRepository;
use AppBundle\Repository\QCMsRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

class QCM
{
    private $qcmRepository;
    private $questionRepository;
    private $googleAPI;

    public function __construct(QCMsRepository $qcmRepository, QCMsQuestionsRepository $questionRepository, $googleAPI)
    {
        $this->qcmRepository             = $qcmRepository;
        $this->questionRepository        = $questionRepository;
        $this->googleAPI = $googleAPI;
    }

    /**
     * @param array $data
     * @return float
     * @throws Exception
     */
    public function calculateQCMResult($data) {
        $result = 0;
        unset($data['CGU']);
        foreach ($data as $name => $answer) {
            $trueValue = null;
            $id = str_replace('question_','',$name);
            /** @var Question $question */
            $question = $this->questionRepository->find($id);
            if($question->getType() == 'multiple') {
                foreach ($question->getResponses() as $response) {
                    if($response->getIsTrue()) {
                        $trueValue[] = $response->getId();
                    }
                }
                $isTrueAnswer = true;
                foreach ($answer as $res) {
                    if(!in_array($res,$trueValue)) {
                        $isTrueAnswer = false;
                        break;
                    }
                }
                if($isTrueAnswer) {
                    $result++;
                }
            } else {
                foreach ($question->getResponses() as $response) {
                    if($response->getIsTrue()) {
                        $trueValue = $response->getId();
                        break;
                    }
                }
                if($trueValue == $answer) { $result++; }
            }
        }
        return $result < 0 ? 0 : $result;
    }

    function geocode($address){

        $address = urlencode($address);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$this->googleAPI}";
        $resp_json = file_get_contents($url);
        $resp = json_decode($resp_json, true);

        if($resp['status'] == 'OK'){

            $lati = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $longi = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
            $formatted_address = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : "";

            if($lati && $longi && $formatted_address){
                $return = [
                    'latitude' => $lati,
                    'longitude' => $longi,
                    'formatted_address' => $formatted_address
                ];
                foreach ($resp['results'][0]['address_components'] as $components) {
                    if(in_array('postal_code', $components['types'])) {
                        $return['postal_code'] = $components['short_name'];
                    }
                    if(in_array('locality', $components['types'])) {
                        $return['city'] = $components['long_name'];
                    }
                }
                return $return;

            } else {
                return false;
            }

        } else {
            return false;
        }
    }

}