<?php

namespace UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\StreamedResponse;
use UserBundle\Entity\Apprenant;
use UserBundle\Entity\User;
use UserBundle\Repository\ApprenantRepository;
use UserBundle\Repository\UserRepository;

class UsersManager
{
    public  $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function generateCsv(array $filters) {

        /** @var ApprenantRepository $apprenantRepository */
        $apprenantRepository = $this->em->getRepository(Apprenant::class);
        $response = new StreamedResponse();
        $response->setCallback(function() use($apprenantRepository, $filters) {
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, [
                'Code',
                'Nom',
                'Prénom',
                'Date de naissance',
                'Date d\'examen',
                'Résultat'
            ], ';');

            $offset = 0;
            $filters['offset'] = $offset;
            $query = $apprenantRepository->getSearchResult($filters);
            $apprenants = $query->getResult();
            $apprenantsCount = count($apprenants);
            while($apprenantsCount > 0) {
                for ($i = 0; $i < $apprenantsCount; $i++) {
                    $code = is_numeric($apprenants[$i]->getCode()) ? sprintf('%06d', $apprenants[$i]->getCode()) : $apprenants[$i]->getCode();
                    $birth = $apprenants[$i]->getBirthday() ? $apprenants[$i]->getBirthday()->format('Y-m-d') : '-';
                    $data = [
                        $code,
                        $apprenants[$i]->getNom(),
                        $apprenants[$i]->getPrenom(),
                        $birth,
                        $apprenants[$i]->getAttestation()->getDate()->format('Y-m-d'),
                        $apprenants[$i]->getAttestation()->getSuccessed() ? 'Réussite' : 'Echec',
                    ];

                    fputcsv($handle, $data, ';');
                }
                $this->em->clear();
                $filters['offset'] += 2000;
                $query = $apprenantRepository->getSearchResult($filters);
                $apprenants = $query->getResult();
                $apprenantsCount = count($apprenants);
            }
            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="Apprenants.csv"');

        return $response;
    }
}