<?php

namespace UserBundle\Repository;

/**
 * ApprenantRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ApprenantRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return array
     */
    public function getSearchResult(array $data)
    {
        $qb = $this->createQueryBuilder('a');
        if(!empty($data)) {
            if($data['code']) {
                $qb->andWhere($qb->expr()->eq('LOWER(a.code)',':code'))
                    ->setParameter('code',$data['code']);
            } else {
                if($data['nom']) {
                    $qb->andWhere($qb->expr()->like('LOWER(a.nom)',':nom'))
                        ->setParameter('nom', '%'.strtolower($data['nom']).'%');
                }
                if($data['prenom']) {
                    $qb->andWhere($qb->expr()->like('LOWER(a.prenom)',':prenom'))
                        ->setParameter('prenom', '%'.strtolower($data['prenom']).'%');
                }
                if($data['birthday']) {
                    $birthday = $data['birthday']->format('Y').'-'.$data['birthday']->format('m').'-'.$data['birthday']->format('d');
 
                    $qb->andWhere($qb->expr()->eq('DATE(a.birthday)',':birthday'))
                        ->setParameter('birthday', $birthday);

                }
                if($data['exam']) {
                    $qb->leftJoin('a.attestation','att');
                    $_date = $data['exam']->format('Y').'-'.$data['exam']->format('m').'-'.$data['exam']->format('d');

                    $qb->andWhere($qb->expr()->eq('DATE(att.date)',':_date'))
                        ->setParameter('_date', $_date);
                }
                if(isset($data['ett']) && !$data['ett']->isEmpty()) {
                    $e = [];
                    foreach ($data['ett'] as $ett) {
                        $e[] = $ett->getId();
                    }
                    $qb->leftJoin('a.ett','e');
                    $qb->andWhere($qb->expr()->in('e.id',':ett'));
                    $qb->setParameter('ett',$e);
                }
            }
        }
        $qb->leftJoin('a.attestation', 'at')
            ->andWhere($qb->expr()->andX(
                $qb->expr()->isNotNull('at.id'),
                $qb->expr()->isNotNull('at.successed')
            ));

        $date = new \DateTime('now');
        $date->sub(new \DateInterval('P10Y'));
        $qb->andWhere($qb->expr()->gte('at.date',':date'))
            ->setParameter('date',$date);

        $qb->orderBy('a.nom', 'asc');

        if(isset($data['offset'])) {
            $qb->setFirstResult($data['offset']);
            $qb->setMaxResults(2000);
        }
        return $qb->getQuery();
    }

    public function count() {
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getNotifications($user) {
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.attestation','at')
            ->where($qb->expr()->isNotNull('at.id'));

        if($user->hasRole('ROLE_OF')) {
            $qb->leftJoin('a.of','o')
                ->andWhere($qb->expr()->eq('o.id',$user->getId()))
                ->andWhere($qb->expr()->isNull('at.imageOF'))
            ;
        }

        $qb->orderBy('at.date', 'desc');
        return $qb->getQuery()->getResult();
    }

    function getStatistiquesByOF()
    {
        $tmp = [];
        $labels = [];

        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.of','o')
            ->leftJoin('a.attestation','at')
            ->select('COUNT(a) as failed, o.societe')
            ->where('o.societe IS NOT NULL')
            ->andWhere('at.successed = 0')
        ;
        $qb->groupBy('o.societe');
        $result  = $qb->getQuery()->getResult();
        foreach ($result as $item) {
            $tmp[$item['societe']] = [
                'failed' => $item['failed'],
                'successed' => 0
            ];
        }

        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.of','o')
            ->leftJoin('a.attestation','at')
            ->select('COUNT(a) as _Success, o.societe')
            ->where('o.societe IS NOT NULL')
            ->andWhere('at.successed = 1')
        ;
        $qb->groupBy('o.societe');
        $successAgency  = $qb->getQuery()->getResult();
        foreach ($successAgency as $item) {
            $tmp[$item['societe']]['successed'] = $item['_Success'];
            if(!isset($tmp[$item['societe']]['failed'])) { $tmp[$item['societe']]['failed'] = 0; }
        }
        return $tmp;
    }

    /**
     * @return array
     */
    public function getArchivedApprenantsToDelete()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.attestation', 'at')
            ->andWhere($qb->expr()->andX(
                $qb->expr()->isNotNull('at.id'),
                $qb->expr()->isNotNull('at.successed')
            ));

        $date = new \DateTime('now');
        $date->sub(new \DateInterval('P15Y6M'));
        $qb->andWhere($qb->expr()->lt('at.date',':date'))
            ->setParameter('date',$date);

        return $qb->getQuery()->getResult();
    }
}
