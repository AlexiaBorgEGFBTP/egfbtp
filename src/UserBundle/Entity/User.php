<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Form\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(type="string", name="email", length=255, unique=true, nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(type="string", name="email_canonical", length=255, unique=true, nullable=true))
 *     })
 * })
 * @UniqueEntity("email")
 */
class User extends BaseUser
{

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez saisir le nom de société.", groups={"Registration"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="Le nom de la société est très court.",
     *     maxMessage="Le nom de la société est très long.",
     *     groups={"Registration"}
     * )
     */
    protected $societe;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez saisir le numéro de siret.", groups={"Registration"})
     * @Assert\Length(
     *     min=3,
     *     max=25,
     *     minMessage="Le numéro de SIRET est très court.",
     *     maxMessage="Le numéro de SIRET est très long.",
     *     groups={"Registration"}
     * )
     */
    protected $siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez saisir l'adresse.", groups={"Registration"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="l'adresse est très courte.",
     *     maxMessage="l'adresse est très longue.",
     *     groups={"Registration"}
     * )
     */
    protected $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez saisir le code postal.", groups={"Registration"})
     */
    protected $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez saisir la ville.", groups={"Registration"})
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\NotBlank(message="Veuillez saisir votre numéro de téléphone.", groups={"Registration"})
     */
    protected $telephone;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $emailEnabled = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hasToChangePassword = false;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\Apprenant", mappedBy="ett")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $apprenantsETT;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\Apprenant", mappedBy="of")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $apprenantsOF;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SearchPerDay", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $searchs;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank(message="Vous devez accepter les conditions générales d'utilisation pour s'inscrire.", groups={"Registration"})
     */
    private $CGU = false;

    public $recaptcha;

    /**
     * @Assert\Regex(
     *  pattern="/^\S*(?=\S{12,16})(?=\S*[A-Za-z])(?=.*?[#?!@$%^&*-~])(?=\S*[\d])\S*$/",
     *  message="Le mot de passe doit contenir entre 12 et 16 caractères alphanumériques dont une majuscule, un chiffre et un caractère spécial."
     * )
     * @Assert\NotBlank(message="fos_user.password.blank", groups={"Registration","Resetting"})
     *
     */
    protected $plainPassword;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\LoginAttempt", mappedBy="admin")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $authorizedFailedLoginAttempts;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $cguAcceptDate;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $region;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    private $latitude;

    public function __construct()
    {
        parent::__construct();
        $this->emailEnabled = false;
        $this->CGU = false;
        $this->cguAcceptDate = new \DateTime("now");
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return User
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return User
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        if(!$this->getUsername())
            $this->setUsername($email);

        return $this;
    }

    /**
     * Set emailEnabled
     *
     * @param boolean $emailEnabled
     *
     * @return User
     */
    public function setEmailEnabled($emailEnabled)
    {
        $this->emailEnabled = $emailEnabled;

        return $this;
    }

    /**
     * Get emailEnabled
     *
     * @return boolean
     */
    public function getEmailEnabled()
    {
        return $this->emailEnabled;
    }

    /**
     * Add apprenantsETT
     *
     * @param \UserBundle\Entity\Apprenant $apprenantsETT
     *
     * @return User
     */
    public function addApprenantsETT(\UserBundle\Entity\Apprenant $apprenantsETT)
    {
        $this->apprenantsETT[] = $apprenantsETT;

        return $this;
    }

    /**
     * Remove apprenantsETT
     *
     * @param \UserBundle\Entity\Apprenant $apprenantsETT
     */
    public function removeApprenantsETT(\UserBundle\Entity\Apprenant $apprenantsETT)
    {
        $this->apprenantsETT->removeElement($apprenantsETT);
    }

    /**
     * Get apprenantsETT
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApprenantsETT()
    {
        return $this->apprenantsETT;
    }

    /**
     * Add apprenantsOF
     *
     * @param \UserBundle\Entity\User $apprenantsOF
     *
     * @return User
     */
    public function addApprenantsOF(\UserBundle\Entity\Apprenant $apprenantsOF)
    {
        $this->apprenantsOF[] = $apprenantsOF;

        return $this;
    }

    /**
     * Remove apprenantsOF
     *
     * @param \UserBundle\Entity\User $apprenantsOF
     */
    public function removeApprenantsOF(\UserBundle\Entity\Apprenant $apprenantsOF)
    {
        $this->apprenantsOF->removeElement($apprenantsOF);
    }

    /**
     * Get apprenantsOF
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApprenantsOF()
    {
        return $this->apprenantsOF;
    }

    /**
     * Add search
     *
     * @param \AppBundle\Entity\SearchPerDay $search
     *
     * @return User
     */
    public function addSearch(\AppBundle\Entity\SearchPerDay $search)
    {
        $this->searchs[] = $search;

        return $this;
    }

    /**
     * Remove search
     *
     * @param \AppBundle\Entity\SearchPerDay $search
     */
    public function removeSearch(\AppBundle\Entity\SearchPerDay $search)
    {
        $this->searchs->removeElement($search);
    }

    /**
     * Get searchs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSearchs()
    {
        return $this->searchs;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set hasToChangePassword
     *
     * @param boolean $hasToChangePassword
     *
     * @return User
     */
    public function setHasToChangePassword($hasToChangePassword)
    {
        $this->hasToChangePassword = $hasToChangePassword;

        return $this;
    }

    /**
     * Get hasToChangePassword
     *
     * @return boolean
     */
    public function getHasToChangePassword()
    {
        return $this->hasToChangePassword;
    }

    /**
     * Set cGU
     *
     * @param boolean $cGU
     *
     * @return User
     */
    public function setCGU($cGU)
    {
        $this->CGU = $cGU;

        return $this;
    }

    /**
     * Get cGU
     *
     * @return boolean
     */
    public function getCGU()
    {
        return $this->CGU;
    }

    /**
     * Add authorizedFailedLoginAttempt
     *
     * @param \UserBundle\Entity\LoginAttempt $authorizedFailedLoginAttempt
     *
     * @return User
     */
    public function addAuthorizedFailedLoginAttempt(\UserBundle\Entity\LoginAttempt $authorizedFailedLoginAttempt)
    {
        $this->authorizedFailedLoginAttempts[] = $authorizedFailedLoginAttempt;

        return $this;
    }

    /**
     * Remove authorizedFailedLoginAttempt
     *
     * @param \UserBundle\Entity\LoginAttempt $authorizedFailedLoginAttempt
     */
    public function removeAuthorizedFailedLoginAttempt(\UserBundle\Entity\LoginAttempt $authorizedFailedLoginAttempt)
    {
        $this->authorizedFailedLoginAttempts->removeElement($authorizedFailedLoginAttempt);
    }

    /**
     * Get authorizedFailedLoginAttempts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizedFailedLoginAttempts()
    {
        return $this->authorizedFailedLoginAttempts;
    }

    public function getPendingAuthorizedFailedLoginAttempts()
    {
        $attemps = new ArrayCollection();
        if(count($this->getAuthorizedFailedLoginAttempts())) {
            /** @var \UserBundle\Entity\LoginAttempt $attempt */
            foreach ($this->getAuthorizedFailedLoginAttempts() as $attempt) {
                if(!$attempt->getAuthorizedByAdmin() and !$attempt->getSuccessAttempt()) {
                    $attemps->add($attempt);
                }
            }
        }
        return $attemps;
    }

    public function getAuthorisation()
    {
        if(!$this->enabled)
            return 'Accès non autorisé';

        foreach ($this->roles as $role) {
            if($role == 'ROLE_ADMIN') {
                return 'Autorisé à modifier/tester';
            } elseif($role == 'ROLE_ADMIN_VIEWER') {
                return 'Autorisé à consulter';
            }
        }
    }

    /**
     * Set cguAcceptDate
     *
     * @param \DateTime $cguAcceptDate
     *
     * @return User
     */
    public function setCguAcceptDate($cguAcceptDate)
    {
        $this->cguAcceptDate = $cguAcceptDate;

        return $this;
    }

    /**
     * Get cguAcceptDate
     *
     * @return \DateTime
     */
    public function getCguAcceptDate()
    {
        return $this->cguAcceptDate;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return User
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return User
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return User
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return User
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
}
