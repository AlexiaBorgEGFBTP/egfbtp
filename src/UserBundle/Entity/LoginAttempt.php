<?php

namespace UserBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\LoginAttemptRepository")
 * @ORM\Table(name="login_attempts")
 */
class LoginAttempt
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $ip;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    protected $countryCode;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $authorizedByAdmin = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $authorizationDate;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="authorizedFailedLoginAttempts")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $admin;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $successAttempt = false;

    public function __construct($email, $ip,$countryCode, $successAttempt)
    {
        $this->email = $email;
        $this->ip = $ip;
        $this->successAttempt = $successAttempt;
        $this->countryCode = $countryCode;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LoginAttempt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return LoginAttempt
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return LoginAttempt
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set authorizedByAdmin
     *
     * @param boolean $authorizedByAdmin
     *
     * @return LoginAttempt
     */
    public function setAuthorizedByAdmin($authorizedByAdmin)
    {
        $this->authorizedByAdmin = $authorizedByAdmin;

        return $this;
    }

    /**
     * Get authorizedByAdmin
     *
     * @return boolean
     */
    public function getAuthorizedByAdmin()
    {
        return $this->authorizedByAdmin;
    }

    /**
     * Set authorizationDate
     *
     * @param \DateTime $authorizationDate
     *
     * @return LoginAttempt
     */
    public function setAuthorizationDate($authorizationDate)
    {
        $this->authorizationDate = $authorizationDate;

        return $this;
    }

    /**
     * Get authorizationDate
     *
     * @return \DateTime
     */
    public function getAuthorizationDate()
    {
        return $this->authorizationDate;
    }

    /**
     * Set admin
     *
     * @param \UserBundle\Entity\User $admin
     *
     * @return LoginAttempt
     */
    public function setAdmin(\UserBundle\Entity\User $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return \UserBundle\Entity\User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set successAttempt
     *
     * @param boolean $successAttempt
     *
     * @return LoginAttempt
     */
    public function setSuccessAttempt($successAttempt)
    {
        $this->successAttempt = $successAttempt;

        return $this;
    }

    /**
     * Get successAttempt
     *
     * @return boolean
     */
    public function getSuccessAttempt()
    {
        return $this->successAttempt;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return LoginAttempt
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
}
