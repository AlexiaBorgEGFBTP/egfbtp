<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use UserBundle\Repository\UserRepository;
use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;

class ResettingFormType extends AbstractType
{
    /** @var null|\UserBundle\Entity\User  */
    protected $user;

    /**
     * ResettingFormType constructor.
     * @param RequestStack $requestStack
     * @param UserRepository $userRepository
     */
    public function __construct(RequestStack $requestStack, UserRepository $userRepository)
    {
        $request = $requestStack->getCurrentRequest();
        if($request->attributes->has('token')) {
            $token = $request->attributes->get('token');
            $this->user = $userRepository->findOneBy(['confirmationToken' => $token]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
            'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.new_password'),
            'second_options' => array('label' => 'form.new_password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
            'constraints' => [ new Assert\NotBlank(['message' => 'Ce champs est obligatoire.'])]
        ));

        if($this->user->getHasToChangePassword()) {
            $builder
                ->add('recaptcha', EWZRecaptchaType::class, array(
                    'label'      => false,
                    'constraints' => array(
                        new Recaptcha\IsTrue()
                    )
                ))
                ->add('CGU', CheckboxType::class, [
                    'label' => 'J’accepte <a href="/CGU.pdf" target="_blank">les conditions générales d’utilisations</a>',
                    'attr' => [
                        'class' => 'icheck'
                    ],
                    'data' => null
                ]);

            if($this->user->hasRole('ROLE_OF')) {
                $builder
                    ->add('societe',  TextType::class, [
                        'label' => 'Nom de la société',
                        'required' => true,
                        'constraints' => [ new Assert\NotBlank(['message' => 'Ce champs est obligatoire.'])]
                    ])
                    ->add('adresse',  TextType::class, [
                        'label' => 'Adresse',
                        'required' => true,
                        'constraints' => [ new Assert\NotBlank(['message' => 'Ce champs est obligatoire.'])],
                        'attr' => [
                            'class' => 'adresse'
                        ]
                    ])
                    ->add('telephone',  TextType::class, [
                        'label' => 'Téléphone',
                        'required' => true,
                        'constraints' => [ new Assert\NotBlank(['message' => 'Ce champs est obligatoire.'])],
                    ])
                    ->add('codePostal')
                    ->add('city',TextType::class, [
                        'label' => 'Ville',
                    ])
                    ->add('latitude', HiddenType::class, ['attr' => ['class' => 'latitude']])
                    ->add('longitude', HiddenType::class, ['attr' => ['class' => 'longitude']])
                ;
            }
        }
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ResettingFormType';
    }

    public function getName()
    {
        return 'fos_user_resetting';
    }
}
