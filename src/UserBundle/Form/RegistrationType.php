<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;

class RegistrationType extends AbstractType
{

    protected $request;

    /**
     * RegistrationType constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $t = $this->request->query->get('t', null);
        $href = "/CGU.pdf";
        parent::buildForm($builder,$options);
        $builder->remove('username');
        $builder
            ->add('email')
            ->add('societe')
            ->add('siret', null,[
                'label' => 'N° SIRET'
            ])
            ->add('adresse')
            ->add('telephone')
            ->add('CGU', CheckboxType::class, [
                'label' => 'J’accepte <a href="'.$href.'" target="_blank">les conditions générales d’utilisations</a>',
                'attr' => [
                    'class' => 'icheck'
                ]
            ])
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'label'      => false,
                'constraints' => array(
                    new Recaptcha\IsTrue()
                )
            ))
        ;
            $builder
                ->add('codePostal')
                ->add('city',TextType::class, [
                    'label' => 'Ville',
                ]);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}
