<?php

namespace UserBundle\Handlers;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use UserBundle\Entity\LoginAttempt;
use UserBundle\Entity\User;

class AuthenticationHandler implements EventSubscriberInterface
{
    const COUNT_LOGIN_ATTEMPTS = 'login_attempts_key';
    /**
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $requestStack;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $container;
    /**
     * AuthenticationHandler constructor.
     * @param RequestStack $requestStack
     * @param EntityManager $manager
     */
    public function __construct(RequestStack $requestStack, EntityManager $manager, $container)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $manager;
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin'
        ];
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $request = $this->getRequestStack()->getCurrentRequest();
        if($request->getClientIp()) {
            $loginAttemptsRepository = $this->getEntityManager()->getRepository(LoginAttempt::class);
            $username = $request->get('_username', null);
            $currentIP = $request->getClientIp();
            $failedLoginAttempts = $loginAttemptsRepository->findBy(['email' => $username,'ip' => $currentIP, 'successAttempt' => false, 'authorizedByAdmin' => false]);
            $attemptsCount = count($failedLoginAttempts);
            if($attemptsCount < 3) {
                $countryCode = $this->getLocationInfoByIp();
                $loginAttempt = new LoginAttempt($username, $currentIP,$countryCode, false);
                $this->getEntityManager()->persist($loginAttempt);
                $this->getEntityManager()->flush();
            } else {
                $userRepository = $this->getEntityManager()->getRepository(User::class);
                $user = $userRepository->findOneBy(['username' => $username]);
                if($user) {
                    $user->setEnabled(false);
                    $this->getEntityManager()->persist($user);
                    $this->getEntityManager()->flush();
                }
            }
        }
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $request = $this->getRequestStack()->getCurrentRequest();
        $username = $request->get('_username', null);
        $currentIP = $request->getClientIp();
        $loginAttemptsRepository = $this->getEntityManager()->getRepository(LoginAttempt::class);
        $failedLoginAttempts = $loginAttemptsRepository->findBy(['email' => $username,'ip' => $currentIP, 'successAttempt' => false, 'authorizedByAdmin' => false]);
        $attemptsCount = count($failedLoginAttempts);
        if( $attemptsCount < 3) {
            $countryCode = $this->getLocationInfoByIp();
            $loginAttempt = new LoginAttempt($username, $currentIP, $countryCode,true);
            $this->getEntityManager()->persist($loginAttempt);
            /** @var LoginAttempt $failedLoginAttempt */
            foreach ($failedLoginAttempts as $failedLoginAttempt) {
                $failedLoginAttempt->setAuthorizedByAdmin(true);
                $failedLoginAttempt->setAuthorizationDate(new \DateTime("now"));
                $this->getEntityManager()->persist($failedLoginAttempt);
            }
            $userRepository = $this->getEntityManager()->getRepository(User::class);
            $user = $userRepository->findOneBy(['username' => $username]);
            if($user) {
                $user->setEnabled(true);
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();
            }
        }  else {
            $userRepository = $this->getEntityManager()->getRepository(User::class);
            $user = $userRepository->findOneBy(['username' => $username]);
            if($user) {
                $user->setEnabled(false);
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();
            }
            $this->container->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            $lastUsernameKey = Security::LAST_USERNAME;
            $request->getSession()->set($lastUsernameKey,$username);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RequestStack
     */
    public function getRequestStack()
    {
        return $this->requestStack;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    private function getLocationInfoByIp(){
        $request = $this->requestStack->getCurrentRequest();
        $client  = $request->getClientIp();
        $forward = $request->server->get('HTTP_X_FORWARDED_FOR',null);
        $remote = $request->server->get('REMOTE_ADDR',null);
        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        } elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        } else{
            $ip = $remote;
        }
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
        if($ip_data && $ip_data->geoplugin_countryName != null){
            return $ip_data->geoplugin_countryCode;
        }
        return null;
    }
}
